/**
 * @author Luiz Henrique Bueno
 */

(function () {
  // define variables
  var canvas = document.getElementById("canvas");
  var context = canvas.getContext("2d");
  var player = {};
  var road = [];
  var platformWidth = 32;
  var platformHeight = canvas.height - platformWidth * 4;
  /**
   * Request Animation Polyfill
   */
  var requestAnimFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(callback, element){
              window.setTimeout(callback, 1000 / 60);
            };
  })();

/**
 * Asset pre-loader object. Loads all images
 */
var assetLoader = (function() {
  // images dictionary
  this.images = {
    "highway" : "assets/images/highway.png"
  };
  
  var assetsLoaded = 0;                                // how many assets have been loaded
  var numberImages = Object.keys(this.imgs).length;    // total number of image assets
  this.assetsCount = numberImages;                          // total number of assets
  /**
   * Ensure all assets are loaded before using them
   * @param {number} dic  - Dictionary name ('imgs')
   * @param {number} name - Asset name in the dictionary
   */
  function assetLoaded(dic, name) {
    // don't count assets that have already loaded
    if (this[dic][name].status !== "loading" ) {
      return;
    }
    this[dic][name].status = "loaded";
    assetsLoaded++;
    // finished callback
    if (assetsLoaded === this.assetsCount && typeof this.finished === "function") {
      this.finished();
    }
  }
  /**
   * Create assets, set callback for asset loading, set asset source
   */
  this.downloadAll = function() {
    var _this = this;
    var src;
    // load images
    for (var img in this.imgs) {
      if (this.imgs.hasOwnProperty(img)) {
        src = this.imgs[img];
        // create a closure for event binding
        (function(_this, img) {
          _this.imgs[img] = new Image();
          _this.imgs[img].status = "loading";
          _this.imgs[img].name = img;
          _this.imgs[img].onload = function() { assetLoaded.call(_this, "imgs", img) };
          _this.imgs[img].src = src;
        })(_this, img);
      }
    }
  }
  return {
    imgs: this.imgs,
    assetsCount: this.assetsCount,
    downloadAll: this.downloadAll
  };
})();
assetLoader.finished = function() {
  startGame();
}


/**
 * Start the game - reset all variables and entities, spawn platforms and water.
 */
function startGame() {
  // setup the player
  //player.width  = 60;
  //player.height = 96;
  //player.speed  = 6;
  //player.sheet  = new SpriteSheet("imgs/normal_walk.png", player.width, player.height);
  //player.anim   = new Animation(player.sheet, 4, 0, 15);
  // create the ground tiles
  for (i = 0, length = Math.floor(canvas.width / platformWidth) + 1; i < length; i++) {
    road[i] = {"x": i * platformWidth, "y": platformHeight};
  }
  background.reset();
  animate();
}
/**
 * Game loop
 */
function animate() {
  requestAnimFrame( animate );
  background.draw();
  for (i = 0; i < road.length; i++) {
    road[i].x -= player.speed;
    ctx.drawImage(assetLoader.images.road, road[i].x, road[i].y);
  }
  if (road[0].x <= -platformWidth) {
    road.shift();
    road.push({"x": road[ground.length-1].x + platformWidth, "y": platformHeight});
  }
  player.anim.update();
  player.anim.draw(64, 260);
}
assetLoader.downloadAll();

})();
