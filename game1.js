

// define variables
var player, score;
var road = [], enemies = [], environment = [];

var Game = {};
Game.fps = 30;

/**
 * 
 * Background
 */
var background = (function() {
	var road = {};
	var plates = {};
	var trees = {};
	
	this.draw = function() {
		this.context.drawImage(assetLoader.images.highway, 0, 0);
	};
	
	this.reset = function() {
		
	};
	
	return {
		draw: this.draw,
		reset: this.reset
	};
	
})();

/**
 * 
 * Random number function
 **/
 function randNumber(first, last) {
     return Math.floor(Math.random() * (last - first + 1) + first);
 }
 
/**
 *
 * Asset loader function
 **/
var assetLoader = (function() {
    
  this.images = {
    'highway' : 'assets/images/highway.png'
  };

  this.sounds = {
    'background' : 'assets/sounds/background.mp3'
  };

  var numberImages = Object.keys(this.images).length;  
  var numberSounds = Object.keys(this.sounds).length;  
  this.assetsCount = numberImages;                          

  var assetsLoaded = 0;                               
  function assetLoaded(dict, name) {
      
    // Return if asset has already loaded
    if (this[dict][name].status !== 'loading') {
      return;
    }

    this[dict][name].status = 'loaded';
    assetsLoaded++;

    // progress callback
    if (typeof this.progress === 'function') {
      this.progress(assetsLoaded, this.assetsCount);
    }

    // finished callback
    if (assetsLoaded === this.assetsCount && typeof this.finished === 'function') {
      this.finished();
    }
  }

  /**
   * Create assets, set callback for asset loading, set asset source
   */
  this.downloadAssets = function() {
    var _this = this;
    var imageSource;

    // load images
    for (var image in this.images) {
        
      if (this.images.hasOwner(image)) {
        imageSource = this.images[image];

        (function(_this, image) {
          _this.images[image] = new Image();
          _this.images[image].status = 'loading';
          _this.images[image].name = image;
          _this.images[image].onload = function() { assetLoaded.call(_this, 'assets/images', image) };
          _this.images[image].source = imageSource;
        })(_this, image);
      }
    }
  }

  return {
    images: this.images,
    sounds: this.sounds,
    assetsCount: this.assetsCount,
    downloadAssets: this.downloadAssets
  };
})();

/**
 * 
 * Asset load progress
 */
assetLoader.progress = function(progress, total) {
  var progressBar = document.getElementById('progress-bar');
  progressBar.value = progress / total;
  document.getElementById('pBar').innerHTML = Math.round(progressBar.value * 100) + "%";
}

/**
 * 
 * Load menu function
 */
assetLoader.finished = function() {
  mainMenu();
}

/**
 * 
 * Show main menu function
 */
function showMainMenu() {
	
	/*
  for (var sound in assetLoader.sounds) {
    if (assetLoader.sounds.hasOwner(sound)) {
      assetLoader.sounds[sound].muted = !playSound;
    }
  }
*/

  $('#progress').hide();
  $('#main').show();
  $('#menu').addClass('main');
  $('.sound').show();
}

/**
 * Game Initialize
 */
Game.initialize = function() {
  this.entities = [];
  this.context = document.getElementById("canvas").getContext("2d");
  
  if (this.context) {  
  	background.reset();
  	assetLoader.downloadAssets();
  }
};

/**
 * Game Draw
 */
Game.draw = function() {
  this.context.clearRect(0, 0, 800, 600);
  
  background.draw();
};

/*
 * Game Update
 */
Game.update = function() {

};




